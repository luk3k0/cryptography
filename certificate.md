1. Root CA
	- Root certificates are self-signed
	- A root certificate is the top-most certificate of the tree
	- Private key of which is used to "sign" other certificates
	- There are multiple CA

2. Intermediate Certificate
	- Sub CAs are the Certificate Authorities who offers an intermediate root
	- There are multiple intermediate Authorities

3. Certificate signing request (CRS)
	- Message contain applicant's info + applicant's public key
	- Applicant kep private key, send CRS

4. Domain-validated certificate
	- Domain name of the applicant is validated by proving some control over a DNS domain.

5. Certificate revocation list (CRL)
	- List of digital certificates that no longer be trusted.

6. Public key infrastructure (PKI)
	- Set of roles, policies, hardware, software and procedures needed to create, manage, distribute, use, store and revoke digital certificates and manage public-key encryption.

