# 0. Explain in Vinglish 
- Encoding: maintaining data usability and can be reversed by employing the same algorithm that encoded the content, i.e. no key is used.
- Encryption: maintaining data confidentiality and requires the use of a key (kept secret) in order to return to plaintext.
- Hashing: validating the integrity of content by detecting all modification thereof via obvious changes to the hash output.
- Obfuscation: used to prevent people from understanding the meaning of something,
- Symmetric encryption
	+ stream cipher: Converts plain text into cipher by taking 1 byte of plain text at a time.
	+ block cipher: Converts plain text into cipher by taking plain text’s block at a time. Need padding if plain text block != block cipher
	
- Asymmetric encryption
	+ certificate
	+ digital signature


# 1. Symmetric encryption for storage:
- encrypt/decrypt: same key
- Using encryption to en/decrypt data
- manage key, make sure key is secret
- lose key -> data gone forever
```
Alice data --key--> Alice data encrypted
```

# 2. Symmetric encryption for communications: 
- encrypt/decrypt: key
- Using encryption to en/decrypt transfer data
- key rotate
- manage key, make sure key at both side is secret
- weakerness: keep, transfer on both side
```
Alice encrypt  (msg + key = cipher) 
Bob decrypt (cipher + key = msg)
```

# 3. Asymmetric encryption for communications: 
- encrypt: public key; decrypt: private key
- encrypt: private key; decrypt: public key
- 2 key, one way encryption
- NOT with other way
- weakerness: MITM public key

# 4. Certificate: 
- Bind public key to a name
```
Alice bind ( name + pub key = certificate)
```

# 5. Certificates for encrypt communications: 
- encrypt: receiver certificate
- decrypt: receiver private key
```
Alice encrypt (Bob certificate + msg = cipher) 
Bob decrypt (Bob private key + cipher = msg)
```

# 6. Certificates for authentication 
- encrypt: sender private key
- decrypt: sender certificate
```
Alice encrypt (Alice private key + msg = cipher) 
Bob validate (decrypt) (Alice certificate + cipher = msg)
```

# 7. Encrypt + Authentication (5+6)
- encrypt: sender private key, receiver certificate
- decrypt: receiver private key, sender certificate
```
Alice encrypt (( Alice private key + msg) + Bob certificate) = cipher)
Bob decrypt ((Bob private key + cipher) + Alice certificate = msg)
```
# 8. Digital signature
- Mathematical technique used to validate the authenticity and integrity of a message, software or digital document. 
